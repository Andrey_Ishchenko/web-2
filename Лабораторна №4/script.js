function currentDataTime() {
  var date = new Date();
  let dayOfMonth = date.getDate();
  let numOfMonth = date.getMonth();
  let month = getMonth(numOfMonth);
  let year = date.getFullYear();
  let numDayOfWeek = date.getDay();
  let dayOfWeek = getDayOfWeek(numDayOfWeek);
  let hour = date.getHours();
  hour = hour < 10 ? "0" + hour : hour;
  let minute = date.getMinutes();
  minute = minute < 10 ? "0" + minute : minute;
  let sec = date.getSeconds();
  sec = sec < 10 ? "0" + sec : sec;

  var res = "<strong>Дата: </strong>" + dayOfMonth + " " + month + " " + year + " року <br>" +
  "<strong>День: </strong>" + dayOfWeek + "<br>" +
  "<strong>Час: </strong>" + hour + ":" + minute + ":" + sec + "<br>"
  $("#res1").html(res);
}

function getDay() {
  var date = new Date();
  let dayNum = date.getDay();
  let day = getDayOfWeek(dayNum);
  if (dayNum == 0) {
    day = "неділя";
    dayNum = 7;
  }

  var res = "<strong>Номер дня: </strong>" + dayNum + "<br><strong>Назва дня: </strong>" + day + "<br>"
  $("#res2").html(res);
}

function getDayAgo() {
  var n = $("#dayAgo").val();
  if (n != "") {
  let date = new Date();
  date.setDate(date.getDate() - n);
  let dayOfMonth = date.getDate();
  let numOfMonth = date.getMonth();
  let month = getMonth(numOfMonth);
  let year = date.getFullYear();
  $("#res3").html("<strong>Дата: </strong>" + dayOfMonth + " " + month + " " + year + " року <br>");
  $("#dayAgo").val("");
  }
}

function lastDay() {
  var year = $("#year").val();
  var month = $("#month").val();
  if (year != "" && month != "") {
    var date = new Date(year, month, 0);
    date.setMonth(month + 1);
    let day = date.getDate();
    $("#res4").html("<strong>Останє число місяця: </strong>" + day + "<br>");
    $("#year").val(""); $("#month").val("");
  }
}

function calcSec() {
  var d = new Date();
  let sec1 = d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds();
  let sec2 = 86400 - sec1;
  $("#res5").html("<strong>Пройшло секунд: </strong>" + sec1 + "<br>" +
    "<strong>Залишилось секунд: </strong>" + sec2 + "<br>")
}

function convertYear() {
  var year = $("#yearToConvert").val();
  if (year != "") {
    let x = (year % 100) >= 50 ? 2 : 1;
    let c = parseInt(year / 100) + 1;
    let m = parseInt(year / 1000) + 1;
    $("#res6").html(x + " половина " + c + " століття " + m + " тисячоліття");
    $("#yearToConvert").val("");
  }
}

function calcPassedYear() {
  let date1 = $("#year1").val();
  let date2 = $("#year2").val();
  date1 = date1.substr(0, 4);
  date2 = date2.substr(0, 4);
  if (date1 != "" && date2 != "") {
  var res;
  date1 = parseInt(date1);
  date2 = parseInt(date2);
  if (date1 > date2) res = date2 - date1;
  else if (date1 == date2) res = 0;
  else res = date1 - date2;
  res = Math.abs(res);
  $("#res7").html("Пройшло " + res + " повних років");
  $("#year1").val("");
  $("#year2").val("");
  }
}
// The library SunCalc is used here
function calcTime() {
  var time1 = $("#time1").val();
  var time2 = $("#time2").val();
  if (time1 != "" && time2 != "") {
    var times = SunCalc.getTimes(new Date(), 51.28, 0.0); // Grinvich
    let hour1 = parseInt(time1.substr(0, 2));
    let minutes1 = parseInt(time1.substr(3, time1.length));
    let hour2 = parseInt(time2.substr(0, 2));
    let minutes2 = parseInt(time2.substr(3, time2.length));
    var res;
    if (hour1 != times.sunrise.getHours() && hour2 != times.sunrise.getHours()) {
      res = (times.sunrise.getHours() - hour1) + ":";
    } else if (hour1 == times.sunrise.getHours() && hour2 != times.sunrise.getHours()) {
      res = "Error";
    } else if (hour1 != times.sunrise.getHours() && hour2 == times.sunrise.getHours()) {
      res = "Error";
    } else res = "0:";

    if (hour1 != times.sunrise.getHours() && hour2 != times.sunrise.getHours()) {
      res += times.sunrise.getHours() - hour1;
    } else if (hour1 == times.sunrise.getHours() && hour2 != times.sunrise.getHours()) {
      res = "Error";
    } else if (hour1 != times.sunrise.getHours() && hour2 == times.sunrise.getHours()) {
      res = "Error";
    } else res = "0";

    if (res == "0:0") res = "Годиник показує правильний час"
    else res = "Годиник відстає на " + res;

    $("#res8").html(res);
  }
}

function getMonth(m) {
  switch(m){
      case 0: return("Січня"); break;
      case 1: return("Лютого"); break;
      case 2: return("Березеня"); break;
      case 3: return("Квітня"); break;
      case 4: return("Травня"); break;
      case 5: return("Червня"); break;
      case 6: return("Липня"); break;
      case 7: return("Серпня"); break;
      case 8: return("Вересня"); break;
      case 9: return("Жовтня"); break;
      case 10: return("Листопада"); break;
      case 11: return("Грудня"); break;
  }
}
function getDayOfWeek(m) {
  switch(m){
      case 0: return("неділя"); break;
      case 1: return("понеділок"); break;
      case 2: return("вівторок"); break;
      case 3: return("середа"); break;
      case 4: return("четверг"); break;
      case 5: return("п'ятниця'"); break;
      case 6: return("субота"); break;
  }
}
