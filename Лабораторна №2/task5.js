const word = ["myVariable", "newBrandProduct"];

for (let s of word) {
  toSnakeCase(s);
}

function toSnakeCase(s) {
  s = s.replace(/[A-Z]/g, letter => "_" + letter.toLowerCase());
  console.log(s);
}
