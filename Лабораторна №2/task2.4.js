console.log("" + 1 + 0);
// 10 string
console.log("" - 1 + 0);
// -1 integer
console.log(true + false);
// 1 integer
console.log(6 / "3");
// 2 integer
console.log("2" * "3");
// 6 integer
console.log(4 + 5 + "px");
// 9px string
console.log("$" + 4 + 5);
// $45 string
console.log("4" - 2);
// 2 integer
console.log("4px" - 2);
// NaN
console.log(7 / 0);
// Infinity
console.log("  -9\n" + 5);
//    -9
// 5 string
console.log("  -9\n" - 5);
// -14 integer
console.log(5 && 2);
// 2 integer
console.log(2 && 5);
// 5 integer
console.log(5 || 0);
// 5 integer
console.log(0 || 5);
// 5 integer
console.log(null + 1);
// 1 integer
console.log(undefined + 1);
// NaN
console.log(null == "\n0\n");
// false boolean
console.log(+null == +"\n0\n");
// true boolean

// -----------------------------------------------------------------------------

console.log("5" + "1");
// 51 string
console.log("5" + 1);
// 51 string
console.log(5 + 1);
// 6 integer
console.log("5" + true);
// 5true string
console.log(1 / 0);
// Infinity
console.log(-5 / 0);
// -Infinity
console.log(1 / Infinity);
// 0 integer
console.log(Infinity - Infinity);
// NaN
console.log(Infinity + 10);
// Infinity
console.log(Infinity + Infinity);
// Infinity
console.log(Infinity + "3");
// Infinity3 string
console.log(undefined + 5);
// NaN
console.log(parseInt("10"));
// 10 integer
console.log(parseInt("10.3"));
// 10 integer
console.log(parseInt(10.3));
// 10 integer
console.log(parseInt("34 38 23"));
// 34 integer
console.log(parseInt("   60   "));
// 60 integer
console.log(parseInt("x10"));
// NaN
console.log(parseInt("0x10"));
// 16 integer
console.log(parseInt("010"));
// 10 integer
console.log(parseInt("24", 8));
// 20 integer
console.log(parseInt("FF", 16));
// 255 integer
console.log(parseFloat("0xFF"));
// 0 float
console.log(false && 0);
// false boolean
console.log(0 && false);
// 0 integer
console.log(!0);
// true boolean
console.log(!5);
// false boolean
console.log(1 && 0);
// 0 integer
console.log(1 && false);
// false boolean
console.log(1 || 5);
// 1 integer
console.log(0 || 5);
// 5 integer
console.log(5 + 4 + "!");
// 9! string
console.log(!null);
// true boolean
console.log(!!null);
// false boolean
console.log(!!5);
// true boolean
console.log(null == 0);
// false boolean
console.log(null === 0);
// false boolean
console.log(null > -5);
// true boolean
console.log(undefined > 5);
// false boolean
console.log(undefined > 5 != true);
// true boolean
console.log(a = null + 5);
// Exception
