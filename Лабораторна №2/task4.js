const word = ["my_variable", "new_brand_product"];

for (let s of word) {
  toCamelCase(s);
}

function toCamelCase(s) {
  s = s.replace(/([_][a-z])/ig, ($1) => {
    return $1.toUpperCase()
      .replace('_', '');
    });
  console.log(s);
}
