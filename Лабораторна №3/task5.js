var arr = [5, 2, 1, -10, 8];

reverseSort(arr);
document.write("Task 5:  " + arr + "<br><br>");

function reverseSort(arr) {
  arr.sort((a, b) => b - a);
}
