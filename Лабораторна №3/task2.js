var session = [{
    subject: "Веб технології",
    grade: 100,
    status: "Зараховано"
  },
  {
    subject: "Конструювання ПЗ",
    grade: 55,
    status: "Не зараховано"
  },
  {
    subject: "Іноземна мова",
    grade: 40,
    status: "Не зараховано"
  },
  {
    subject: "Комп'ютерна графіка",
    grade: 100,
    status: "Зараховано"
  }
];

document.write("Task 2: <br>")
showList();
addListItem("Бази даних", 80);
addListItem("Комп'ютерна графіка", 55);
showList();
averageGrade();
countNotCreditedSubject();
hightGrade();
findSubjectByGrade(55);

var sortByStatus = function(a) {
  if (a.status == "Зараховано") return -1;
  else return 1;
}

function getStatus(grade) {
  if (grade >= 60) return "Зараховано";
  else return "Не зараховано";
}

 function searchSubject(sub) {
  for (let i = 0; i < session.length; i++) {
    if (session[i].subject == sub) return i;
  }
  return session.length;
}
// 1
function showList() {
  session.sort(sortByStatus);
  for (let str of session) {
    document.write(str.subject + " " + str.grade + " " + str.status + "<br>");
  }
}
// 2
function addListItem(subject, grade) {
  var pos = searchSubject(subject)
  session[pos] = {
    subject: subject,
    grade: grade,
    status: getStatus(grade)
  }
}
// 3
function averageGrade() {
  var grade = 0;
  for (let str of session) {
    grade += str.grade;
  }
  return "Середній бал: " + grade / session.length;
}
// 4
function countNotCreditedSubject() {
  var count = 0;
  for (let str of session) {
    if (str.status == "Не зараховано") count++;
  }
  return "Кількість не зарахованих предметів: " + count;
}
// 5
function hightGrade() {
  var maxGrade = 0;
  var subject = [];
  for (let str of session) {
    if (maxGrade < str.grade) {
      subject = [];
      maxGrade = str.grade;
      subject.push(str);
    } else if (maxGrade == str.grade) {
      subject.push(str);
    }
  }
  return subject;
}
//6
function findSubjectByGrade(grade) {
  var sub = [];
  for (let str of session) {
    if (str.grade == grade) sub.push(str);
  }
  return sub;
}
