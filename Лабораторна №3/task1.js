var obj = {
  className: 'open menu'
}

addClass(obj, 'new');
addClass(obj, 'open');
addClass(obj, 'me');
document.write("Task 1:   " + obj.className + "<br> <br>");

function addClass(obj, cls) {
  var newObj = {};
  var arr = obj.className ? obj.className.split(" ") : [];
  for (var i = 0; i < arr.length; i++) {
    newObj[arr[i]] = true;
  }
  newObj[cls] = true;
  arr = Object.keys(newObj);
  obj.className = arr.join(" ");
}
