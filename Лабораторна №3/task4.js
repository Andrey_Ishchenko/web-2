var arr = [5, 3, 8, 1];

function filterRangeInPlace(arr, a, b) {
  for (var i = 0; i < arr.length; i++) {
    if (a <= arr[i] && arr[i] <= b) {
      arr.splice(i, 1);
      i--;
    }
  }
}

filterRangeInPlace(arr, 1, 4);

document.write("Task 4:  " + arr + "<br><br>")
