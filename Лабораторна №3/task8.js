var vasya = { name: "Вася", age: 23 };
var masha = { name: "Маша", age: 18 };
var vovochka = { name: "Вовочка", age: 6 };

var people = [ vasya , masha , vovochka ];
sortByAge(people);
 // тепер people: [vovochka, masha, vasya]
document.write("Task 8:   " + people[0].age + "<br><br>") // 6

function sortByAge(arr) {
  arr.sort((a, b) => a.age - b.age);
}
