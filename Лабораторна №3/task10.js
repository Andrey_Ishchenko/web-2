var strings = ["C++", "C#", "C++", "C#",
  "C", "C++", "JavaScript", "C++", "JavaScript"
];

document.write("Task 10:   " + unique(strings));

function unique(arr) {
  let temp = [];
  for (let str of arr) {
    if (!temp.includes(str)) temp.push(str);
  }
  return temp;
}
