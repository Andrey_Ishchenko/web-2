var dog1 = new Dog("Rex", "Alsatian", 0.5);
var dog2 = new Dog("Boby", "Bulldog", 2);
var dog3 = new Dog("Dino", "Dachshund", 5);
document.write("Task 9: <br>");
document.write(dog1.voise() + "<br>");
document.write(dog2.voise() + "<br>");
document.write(dog3.voise() + "<br>");
document.write("<br>");

function Dog(name, breed, age) {
  this.name = name;
  this.breed = breed;
  this.age = age;
  this.voise = function() {
    if(age < 1) return "Тяф";
    else if (age < 3) return "Гав";
    else return "Ppp";
  }
}
