var obj = {
  className: 'open menu'
}
removeClass(obj, 'open');
removeClass(obj, 'blabla');
document.write("<br>Task 3: " + obj.className + "<br><br>");

function removeClass(obj, cls) {
  var arr = obj.className ? obj.className.split(" ") : [];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] == cls) {
      arr.splice(i, 1);
      i--;
    }
  }
  obj.className = arr.join(' ');
}
